﻿using Joh.Games;
using UnityEngine;
using UnityEngine.AI;

namespace Joh.PlayerControllers
{
    /// <summary>
    /// The player controller of the game Item Collector
    /// </summary>
    public class ItemCollectorPlayerController : MonoBehaviour
    {
        #region Public Parameters

        /// <summary>
        /// The navigator mesh agent to to move the player to the scene
        /// </summary>
        [Tooltip("The navigator mesh agent to to move the player to the scene")]
        public NavMeshAgent agent;

        /// <summary>
        /// The player camera
        /// </summary>
        [Tooltip("The player camera")] public Camera camera;

        /// <summary>
        /// Target tag to get the target in the scene
        /// </summary>
        [Tooltip("Target tag to get the target in the scene")]
        public string targetTag = "Target";

        #endregion

        #region Protected Parameters

        // Auxiliary variables used to move player to the new position
        protected Ray ray;
        protected RaycastHit hit;

        #endregion

        #region Protected Methods

        /// <summary>
        /// This method is executed each frame
        /// </summary>
        protected void Update()
        {
            if (Input.touchCount == 1)
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        Move(touch.position);
                    }
                }
            }

            if (Input.GetMouseButtonDown(0)) Move(Input.mousePosition);
        }

        /// <summary>
        /// This method is executed when the application starts its execution
        /// </summary>
        protected void Start()
        {
            SetAllComponents();
        }

        /// <summary>
        /// Method executed when the component is reset in editor
        /// </summary>
        protected void Reset()
        {
            SetAllComponents();
        }

        /// <summary>
        /// Find and assign all component required in class
        /// </summary>
        protected void SetAllComponents()
        {
            if (agent == null) agent = GetComponent<NavMeshAgent>();
            if (camera == null) camera = Camera.main;
        }

        /// <summary>
        /// This method detect if this game object overlaps with other.
        /// Is used to get the targets
        /// </summary>
        /// <param name="other"></param>
        protected void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals(targetTag))
            {
                ItemCollectorGame.GetInstance().NewTargetAchieved(other.gameObject);
            }
        }

        /// <summary>
        /// This method allow move the player in the scene using nav mesh agent
        /// </summary>
        /// <param name="newPosition">The new position</param>
        protected void Move(Vector3 newPosition)
        {
            ray = camera.ScreenPointToRay(newPosition);
            if (Physics.Raycast(ray, out hit, 10000))
            {
                agent.SetDestination(hit.point);
            }
        }

        #endregion
    }
}