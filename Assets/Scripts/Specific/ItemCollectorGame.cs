﻿using System;
using System.Linq;
using Joh.Utils;
using UnityEngine;

namespace Joh.Games
{
    /// <summary>
    /// The game singleton class which contains the logic of the game Item Collector
    /// </summary>
    public class ItemCollectorGame : MonoBehaviour
    {
        #region Public Parameters

        /// <summary>
        /// The targets to collect the player in the game
        /// </summary>
        [Tooltip("The targets to collect the player in the game")]
        public GameObject[] targets;

        /// <summary>
        /// The scene to load when the player won the game
        /// </summary>
        [Tooltip("The scene to load when the player won the game")]
        public string sceneNameToLoadWhenPlayerWin = "";
        
        #endregion
        
        #region Public Parameters

        /// <summary>
        /// The singleton instance to call the methods of the class
        /// </summary>
        protected static ItemCollectorGame instance;

        /// <summary>
        /// The number of the target not collected
        /// </summary>
        protected byte numbeOfTargetsAlive;
        
        #endregion
        
        #region public Methods

        /// <summary>
        /// Get the instance of the ItemCollectorGame class
        /// </summary>
        /// <returns>The instance of ItemCollectorGame class</returns>
        public static ItemCollectorGame GetInstance()
        {
            if(!Exist()) throw new Exception("ItemCollectorGame not have instance");
            return instance;
        }

        /// <summary>
        /// Check if exist an instance of the class
        /// </summary>
        /// <returns>If exists an instance of the class</returns>
        public static bool Exist()
        {
            return instance != null;
        }

        /// <summary>
        /// Method to call when a target is collected by the player
        /// </summary>
        /// <param name="target">The target collected</param>
        public void NewTargetAchieved(GameObject target)
        {
            if (targets.Contains(target))
            {
                Destroy(target);
                --numbeOfTargetsAlive;
            }
            CheckIfPlayerWin();
        }
        
        #endregion
        
        #region Protected Methods
        
        /// <summary>
        /// This method is executed when the application starts its execution
        /// </summary>
        protected void Start()
        {
            // Singleton operation
            if (!Exist())
            {
                instance = this;
            }
            
            // Game
            numbeOfTargetsAlive = (byte) targets.Length;
        }

        /// <summary>
        /// Check if the player won the game and  teleport to other scene
        /// </summary>
        protected void CheckIfPlayerWin()
        {
            if(numbeOfTargetsAlive == 0) 
                SceneLoader.StaticLoadSceneByName(sceneNameToLoadWhenPlayerWin);
        }

        #endregion
    }
}