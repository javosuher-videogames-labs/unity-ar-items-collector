﻿using UnityEngine;

namespace Joh.Utils
{
    /// <summary>
    /// This class has useful methods to use in the game
    /// </summary>
    public class Application : MonoBehaviour
    {
        #region Public Methods

        /// <summary>
        /// Exit the game
        /// </summary>
        public static void ExitApplication()
        {
            StaticExitApplication();
        }

        /// <summary>
        /// Exit the game
        /// </summary>
        public static void StaticExitApplication()
        {
            UnityEngine.Application.Quit();
        }

        #endregion
    }
}