﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Joh.Utils
{
    /// <summary>
    /// Class used to load and teleport to another Unity scenes
    /// </summary>
    public class SceneLoader : MonoBehaviour
    {
        #region Public Parameters

        /// <summary>
        /// Scene names to load in the game
        /// </summary>
        [Tooltip("Scene names to load in the game")]
        public string[] sceneNames;

        #endregion

        #region Public Methods

        /// <summary>
        /// Load a scene using the string stored in "sceneNames"
        /// </summary>
        /// <param name="arrayIndex">The index of scene name stored in "sceneNames"</param>
        public void LoadSingleSceneInSceneNames(int arrayIndex)
        {
            StaticLoadSceneByName(sceneNames[arrayIndex]);
        }
        
        /// <summary>
        /// Load a new scene with the current one using the string stored in "sceneNames" 
        /// </summary>
        /// <param name="arrayIndex"></param>
        public void LoadAdditiveSceneInSceneNames(int arrayIndex)
        {
            StaticLoadSceneByName(sceneNames[arrayIndex], LoadSceneMode.Additive);
        }
        
        /// <summary>
        /// Load a scene by name 
        /// </summary>
        /// <param name="sceneName">The scene name</param>
        /// <param name="loadMode">The mode to load the new scene</param>
        public void LoadSceneByName(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Single)
        {
            StaticLoadSceneByName(sceneName, loadMode);
        }

        /// <summary>
        /// Load a scene by name 
        /// </summary>
        /// <param name="sceneName">The scene name</param>
        /// <param name="loadMode">The mode to load the new scene</param>
        public static void StaticLoadSceneByName(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Single)
        {
            SceneManager.LoadScene(sceneName, loadMode);
        }

        #endregion
    }
}